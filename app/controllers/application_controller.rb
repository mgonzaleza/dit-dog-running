# frozen_string_literal: true

# Applications controller
class ApplicationController < ActionController::API
  extend Memoist
  include Api::Errors
  include Api::Pagination

  rescue_from ActiveRecord::RecordNotFound, with: ->(e) { not_found(e, e.model) }
  rescue_from ActionController::ParameterMissing, with: :unprocessable
end

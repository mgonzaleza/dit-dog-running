# frozen_string_literal: true

module Api
  # Api Pagination
  module Pagination
    PAGE_PARAMS = %i[offset limit].freeze
    DEFAULTS = {
      sort: ['name-desc'],
      offset: 0,
      limit: 10
    }.freeze

    private

    def validate_page_params
      invalid_params = page_params.except(*PAGE_PARAMS)
      return bad_request(invalid_paging_message(invalid_params.keys)) if invalid_params.present?
    end

    def next_page_path
      request.path + '?' + next_params
    end

    def previous_page_path
      request.path + (previous_params.empty? ? '' : "?#{previous_params}")
    end

    def page_params
      default_params.merge(params.to_unsafe_h[:page] || {}).transform_values(&:to_i)
    end

    def default_params
      ActionController::Parameters.new(offset: DEFAULTS[:offset], limit: DEFAULTS[:limit]).permit(PAGE_PARAMS)
    end

    def next_params
      base_params.merge(page: page_params.merge(offset: next_offset)).permit!.to_query
    end

    def next_offset
      page_params[:offset] + page_params[:limit]
    end

    def previous_params
      return base_params.deep_dup.permit!.to_query unless params.dig(:page, :offset)

      base_params.merge(page: page_params.merge(offset: previous_offset)).permit!.to_query
    end

    def previous_offset
      [page_params[:offset] - page_params[:limit], 0].max
    end

    def base_params
      @base_params ||= {}
    end
  end
end

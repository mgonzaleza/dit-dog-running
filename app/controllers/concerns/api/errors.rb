# frozen_string_literal: true

module Api
  # Api > Errors
  module Errors
    NOT_FOUND_MSG = 'The requested resource could not be found.'

    private

    def bad_request(exception = 'Bad request')
      errors = Array(exception).map { |e| { detail: e, title: 'Bad Request', code: '400' } }
      render(json: { errors: errors }, status: :bad_request)
    end

    def not_found(exception = NOT_FOUND_MSG, resource = nil)
      # Override exception message to obscure error a bit
      if exception.is_a?(ActiveRecord::RecordNotFound)
        exception = format_not_found(resource)
      end
      render(json: { errors: [{ detail: exception, title: 'Not Found', code: '404' }] },
             status: :not_found)
    end

    def format_not_found(resource)
      return NOT_FOUND_MSG if resource.nil?

      "The requested #{resource.titleize} resource could not be found."
    end

    def unprocessable(exception = 'Unable to process request.', source = nil)
      # Will parse exception message if called from rescue_from
      errors = Array(exception).map do |e|
        { detail: format_unprocessable(e), title: 'Unprocessable Entity', code: '422' }.tap do |error|
          error[:source] = source.class.name unless source.nil?
        end
      end
      render(json: { errors: errors }, status: :unprocessable_entity)
    end

    def format_unprocessable(error)
      error_class = error.class.name
      msg = error.to_s
      msg = msg.ends_with?('.') ? msg : msg + '.'

      if error_class == 'ArgumentError'
        msg = 'Attribute value: ' + msg
      end
      msg.first.capitalize + msg.slice(1..-1)
    end
  end
end

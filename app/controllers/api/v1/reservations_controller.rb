# frozen_string_literal: true

module Api
  module V1
    # API > V1 > ReservationsController
    class ReservationsController < ApplicationController
      before_action :set_reservation, only: %i[show update destroy]

      # GET /reservations
      def index
        @reservations = Reservation.all

        render json: Api::V1::ReservationSerializer.new(@reservations).serializable_hash
      end

      # GET /reservations/1
      def show
        render json: Api::V1::ReservationSerializer.new(@reservation).serializable_hash
      end

      # POST /reservations
      def create
        @reservation = reservation.new(reservation_params)

        if @reservation.save
          render(
            json: Api::V1::ReservationSerializer.new(@reservation).serializable_hash,
            status: :created
          )
        else
          unprocessable('Unable to process request', @reservation)
        end
      end

      # PATCH/PUT /reservations/1
      def update
        if @reservation.update(reservation_params)
          render(
            json: Api::V1::ReservationSerializer.new(@reservation).serializable_hash,
            status: :ok
          )
        else
          unprocessable('Unable to process request', @reservation)
        end
      end

      # DELETE /reservations/1
      def destroy
        @reservation.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_reservation
        @reservation = reservation.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def reservation_params
        params.require(:reservation).permit(:days, :dog_id)
      end
    end
  end
end

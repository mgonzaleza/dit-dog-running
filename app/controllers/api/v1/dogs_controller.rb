# frozen_string_literal: true

module Api
  module V1
    # API > V1 > DogsController
    class DogsController < ApplicationController
      before_action :set_dog, only: %i[show update destroy]

      # GET /dogs
      def index
        @dogs = Dog.all

        render json: Api::V1::DogSerializer.new(@dogs).serializable_hash
      end

      # GET /dogs/1
      def show
        render json: Api::V1::DogSerializer.new(@dog).serializable_hash
      end

      # POST /dogs
      def create
        @dog = Dog.new(dog_params)

        if @dog.save
          render(
            json: Api::V1::DogSerializer.new(@dog).serializable_hash,
            status: :created
          )
        else
          unprocessable('Unable to process request', @dog)
        end
      end

      # PATCH/PUT /dogs/1
      def update

        if @dog.update(dog_params)
          render(
            json: Api::V1::DogSerializer.new(@dog).serializable_hash,
            status: :ok
          )
        else
          unprocessable('Unable to process request', @dog)
        end
      end

      # DELETE /dogs/1
      def destroy
        @dog.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_dog
        @dog = Dog.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def dog_params
        params.require(:dog).permit(:name, :age, :weight, :breed_id)
      end
    end
  end
end

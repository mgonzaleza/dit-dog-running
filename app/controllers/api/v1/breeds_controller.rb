# frozen_string_literal: true

module Api
  module V1
    # API > V1 > BreedsController
    class BreedsController < ApplicationController
      before_action :set_breed, only: %i[show update destroy]

      # GET /breeds
      def index
        @breeds = breeds

        render json: Api::V1::BreedSerializer.new(@breeds, options).serializable_hash
      end

      # GET /breeds/1
      def show
        render json: Api::V1::BreedSerializer.new(@breed).serializable_hash
      end

      # POST /breeds
      def create
        @breed = Breed.new(breed_params)

        if @breed.save
          render(
            json: Api::V1::BreedSerializer.new(@breed).serializable_hash,
            status: :created
          )
        else
          unprocessable('Unable to process request', @breed)
        end
      end

      # PATCH/PUT /breeds/1
      def update
        if @breed.update(breed_params)
          render(
            json: Api::V1::BreedSerializer.new(@breed).serializable_hash,
            status: :ok
          )
        else
          unprocessable('Unable to process request', @breed)
        end
      end

      # DELETE /breeds/1
      def destroy
        @breed.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_breed
        @breed = Breed.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def breed_params
        params.require(:breed).permit(:name)
      end

      memoize def total_breeds
        Breed.all
      end

      memoize def breeds
        total_breeds.limit(page_params[:limit])
      end

      memoize def options
        { meta: { total: breeds.count } }
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module V1
    # Breed Serializer
    class BreedSerializer < ActiveModel::Serializer
      include FastJsonapi::ObjectSerializer

      cache_options enabled: true, cache_length: 12.hours

      attributes :id, :name

      has_many :dogs,
               links: {
                 related: ->(breed) { "/api/v1/dogs(filter: { breed_id: #{breed.id.to_i} }) NOT IMPLEMENTED" }
               } do |breed| # rubocop:disable Style/SymbolProc
                 breed.dogs
               end
    end
  end
end

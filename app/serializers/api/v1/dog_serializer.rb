# frozen_string_literal: true

module Api
  module V1
    # Dog Serializer
    class DogSerializer < ActiveModel::Serializer
      include FastJsonapi::ObjectSerializer

      attributes :id, :name, :age, :weight

      belongs_to :breed,
                links: {
                  related: ->(dog) { "/api/v1/breeds/#{dog.breed.id}" }
                } do |dog| # rubocop:disable Style/SymbolProc
                  dog.breed
                end
    end
  end
end

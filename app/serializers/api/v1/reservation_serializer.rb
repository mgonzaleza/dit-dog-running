# frozen_string_literal: true

module Api
  module V1
    c
    class ReservationSerializer < ActiveModel::Serializer
      include FastJsonapi::ObjectSerializer

      attributes :id, :days

      belongs_to :reservation,
                links: {
                  related: ->(reservation) { "/api/v1/reservations/#{reservation.dog.id}" }
                } do |reservation| # rubocop:disable Style/SymbolProc
                  reservation.dog
                end
    end
  end
end

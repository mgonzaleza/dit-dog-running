# frozen_string_literal: true

# Reservation
class Reservation < ApplicationRecord
  belongs_to :dog

  validates :days, numericality: { greater_than_or_equal_to: 0, allow_nil: false }
end

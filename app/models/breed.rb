# frozen_string_literal: true

# Breeds
class Breed < ApplicationRecord
  has_many :dogs

  validates :name, presence: true, uniqueness: true

  def self.cache_key(breeds)
    # Idea here:  cache key that will get updated when data change
    # Whenever a post get updated, the result of breeds.maximum(:updated_at) 
    # will be changed resulting in a new key
    {
      serializer: Api::V1::BreedSerializer,
      stat_record: breeds.maximum(:updated_at)
    }
  end
end

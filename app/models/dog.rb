# frozen_string_literal: true

# Dogs
class Dog < ApplicationRecord
  belongs_to :breed, touch: true

  validates :name, presence: true, uniqueness: true
  validates :age, numericality: { greater_than_or_equal_to: 0, allow_nil: true }
  validates :weight, numericality: { greater_than_or_equal_to: 0, allow_nil: true }
end

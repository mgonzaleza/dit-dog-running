# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :dogs
      resources :breeds
      resources :reservations
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::BreedsController, type: :request do
  subject(:request_response) do
    request
    response
  end

  let!(:breed_1) { create :breed, name: 'breed1' }
  let!(:breed_2) { create :breed, name: 'breed2' }
  let!(:breed_3) { create :breed, name: 'breed3' }
  let!(:dog_1) { create :dog, breed: breed_1, name: 'Luke', age: 1 }
  let!(:dog_2) { create :dog, name: 'Han' }

  let(:parsed_body) { JSON.parse(request_response.body).deep_symbolize_keys }
  let(:default_params) { {} }
  let(:params) { default_params }

  describe 'GET#index /breeds' do
    let(:request) { get api_v1_breeds_path(params: params) }

    it 'returns breed list' do
      expect(parsed_body[:data]).to include(
        a_hash_including(
          id: breed_1.id.to_s,
          type: 'breed'
        ),
        a_hash_including(
          id: breed_2.id.to_s,
          type: 'breed'
        ),
        a_hash_including(
          id: breed_3.id.to_s,
          type: 'breed'
        )
      )
    end

    it 'returns the relationship with dogs' do
      breed1 = parsed_body[:data].find { |breed| breed[:id] == breed_1.id.to_s }
      expect(breed1[:relationships][:dogs][:data][0][:id]).to eql(dog_1.id.to_s)
    end
  end

  describe 'GET#show /breeds/{id}' do
    let(:request) { get api_v1_breed_path(id: breed_1.id, params: params) }

    it 'returns the breed' do
      expect(parsed_body[:data]).to include(
        a_hash_including(
          id: breed_1.id.to_s,
          type: 'breed'
        )
      )
    end
  end

  describe 'POST#create /breeds' do
    let(:params) { create_params }
    let(:create_params) do
      default_params.merge(
        breed: {
          name: 'test'
        }
      )
    end

    let(:request) { post api_v1_breeds_path(params: params) }

    it 'the breed is created' do
      expect { request_response }.to change(Breed, :count).by(1)
    end

    context 'with required param missing' do
      let(:create_params) { {} }

      it { is_expected.to have_http_status :unprocessable_entity }
    end
  end

  describe 'PATCH#update /breed/{id}' do
    let(:params) { update_params }
    let(:update_params) do
      default_params.merge(
        breed: {
          name: 'new name'
        }
      )
    end

    let(:request) { patch api_v1_breed_path(id: breed_1.id, params: params) }

    it 'the breed is updated' do
      subject
      expect(breed_1.reload.name).to eq('new name')
    end

    context 'with required param missing' do
      let(:update_params) { {} }

      it { is_expected.to have_http_status :unprocessable_entity }
    end
  end

  describe 'DELETE /breeds/{id}' do
    let(:request) { delete api_v1_breed_path(id: breed_1.id, params: params) }

    it 'the breed is deleted' do
      expect { request_response }.to change(Breed, :count).by(-1)
    end

    context 'with invalid param' do
      let(:request) { delete api_v1_breed_path(id: 999, params: params) }

      it { is_expected.to have_http_status :not_found }
    end
  end
end

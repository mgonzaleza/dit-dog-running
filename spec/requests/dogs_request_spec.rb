# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::DogsController, type: :request do
  subject(:request_response) do
    request
    response
  end

  let!(:breed_1) { create :breed, name: 'breed1' }
  let!(:dog_1) { create :dog, breed: breed_1, name: 'Luke', age: 1 }
  let!(:dog_2) { create :dog, name: 'Han' }
  let!(:dog_3) { create :dog, name: 'Leia' }

  let(:parsed_body) { JSON.parse(request_response.body).deep_symbolize_keys }
  let(:default_params) { {} }
  let(:params) { default_params }

  describe 'GET#index /dogs' do
    let(:request) { get api_v1_dogs_path(params: params) }

    it 'returns dog list' do
      expect(parsed_body[:data]).to include(
        a_hash_including(
          id: dog_1.id.to_s,
          type: 'dog'
        ),
        a_hash_including(
          id: dog_2.id.to_s,
          type: 'dog'
        ),
        a_hash_including(
          id: dog_3.id.to_s,
          type: 'dog'
        )
      )
    end

    it 'returns the relationship with breeds' do
      dog1 = parsed_body[:data].find { |dog| dog[:id] == dog_1.id.to_s }
      expect(dog1[:relationships][:breed][:data][:id]).to eql(breed_1.id.to_s)
    end
  end

  describe 'GET#show /dogs/{id}' do
    let(:request) { get api_v1_dog_path(id: dog_1.id, params: params) }

    it 'returns the dog' do
      expect(parsed_body[:data]).to include(
        a_hash_including(
          id: dog_1.id.to_s,
          type: 'dog'
        )
      )
    end
  end

  describe 'POST#create /dogs' do
    let(:params) { create_params }
    let(:create_params) do
      default_params.merge(
        dog: {
          name: 'test',
          breed_id: breed_1.id
        }
      )
    end

    let(:request) { post api_v1_dogs_path(params: params) }

    it 'the breed is created' do
      expect { request_response }.to change(Dog, :count).by(1)
    end

    context 'with required param missing' do
      let(:create_params) { {} }

      it { is_expected.to have_http_status :unprocessable_entity }
    end
  end

  describe 'PATCH#update /dog/{id}' do
    let(:params) { update_params }
    let(:update_params) do
      default_params.merge(
        dog: {
          name: 'Chewie'
        }
      )
    end

    let(:request) { patch api_v1_dog_path(id: dog_1.id, params: params) }

    it 'the dog is updated' do
      subject
      expect(dog_1.reload.name).to eq('Chewie')
    end

    context 'with required param missing' do
      let(:update_params) { {} }

      it { is_expected.to have_http_status :unprocessable_entity }
    end
  end

  describe 'DELETE /dogs/{id}' do
    let(:request) { delete api_v1_dog_path(id: dog_1.id, params: params) }

    it 'the dog is deleted' do
      expect { request_response }.to change(Dog, :count).by(-1)
    end

    context 'with invalid param' do
      let(:request) { delete api_v1_dog_path(id: 999, params: params) }

      it { is_expected.to have_http_status :not_found }
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::BreedSerializer do
  subject(:serialized_breed) { described_class.new(breed).serializable_hash }

  let(:breed) { create :breed }
  let!(:dog) { create :dog, breed: breed }

  let(:expected_attributes) do
    {
      id: breed.id,
      name: breed.name
    }
  end

  let(:expected_relationships) do
    {
      dogs: {
        data: [{
          id: dog.id.to_s,
          type: :dog
        }],
        links: {
          related: "/api/v1/dogs(filter: { breed_id: #{breed.id} }) NOT IMPLEMENTED"
        }
      }
    }
  end

  let(:expected_response) do
    {
      data: a_hash_including(
        id: breed.id,
        type: :breed,
        attributes: expected_attributes,
        relationships: expected_relationships
      )
    }
  end

  context 'with no params' do
    it { is_expected.to include(expected_response) }

    it 'has the correct attributes' do
      expect(serialized_breed.dig(:data, :attributes)).to eq expected_attributes
    end

    it 'has the correct relationships' do
      expect(serialized_breed.dig(:data, :relationships)).to eq expected_relationships
    end
  end
end

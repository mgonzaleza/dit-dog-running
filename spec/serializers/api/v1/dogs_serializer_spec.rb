# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::DogSerializer do
  subject(:serialized_dog) { described_class.new(dog).serializable_hash }

  let(:breed) { create :breed }
  let!(:dog) { create :dog, breed: breed, age: 3, weight: 5 }

  let(:expected_attributes) do
    {
      id: dog.id,
      name: dog.name,
      age: dog.age,
      weight: dog.weight
    }
  end

  let(:expected_relationships) do
    {
      breed: {
        data: {
          id: breed.id.to_s,
          type: :breed
        },
        links: {
          related: "/api/v1/breeds/#{dog.breed.id}"
        }
      }
    }
  end

  let(:expected_response) do
    {
      data: a_hash_including(
        id: dog.id,
        type: :dog,
        attributes: expected_attributes,
        relationships: expected_relationships
      )
    }
  end

  context 'with no params' do
    it { is_expected.to include(expected_response) }

    it 'has the correct attributes' do
      expect(serialized_dog.dig(:data, :attributes)).to eq expected_attributes
    end

    it 'has the correct relationships' do
      expect(serialized_dog.dig(:data, :relationships)).to eq expected_relationships
    end
  end
end

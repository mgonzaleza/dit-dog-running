# frozen_string_literal: true

require 'rails_helper'

describe Breed, type: :model do
  subject { build(:breed) }
  let(:valid_attributes) { { name: 'breed name' } }

  it 'can be instantiated' do
    expect(Breed.new).to be_an_instance_of(Breed)
  end

  describe 'validations' do
    subject { build(:breed) }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
  end
end

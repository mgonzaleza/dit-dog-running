# frozen_string_literal: true

require 'rails_helper'

describe Dog, type: :model do
  subject { build(:dog) }
  let(:valid_attributes) { { name: 'dog name', breed_id: breed.id } }

  it 'can be instantiated' do
    expect(Dog.new).to be_an_instance_of(Dog)
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
  end
end

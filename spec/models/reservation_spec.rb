# frozen_string_literal: true

require 'rails_helper'

describe Reservation, type: :model do
  subject { create(:reservation, days: 1) }
  let(:valid_attributes) { { days: 2, dog_id: dog.id } }

  it 'can be instantiated' do
    expect(Reservation.new).to be_an_instance_of(Reservation)
  end

  describe 'validations' do
    it { is_expected.to validate_numericality_of(:days).is_greater_than(0) }
  end
end

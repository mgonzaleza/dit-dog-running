# frozen_string_literal: true

FactoryBot.define do
  factory :dog do
    name { |i| "dog#{i}" }
    age { nil }
    weight { nil }
    breed
  end
end

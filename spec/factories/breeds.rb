# frozen_string_literal: true

FactoryBot.define do
  factory :breed do
    name { |i| "breed#{i}" }
  end
end

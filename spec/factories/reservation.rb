# frozen_string_literal: true

FactoryBot.define do
  factory :reservation do
    days { 0 }
    dog
  end
end

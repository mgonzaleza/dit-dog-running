# README

This README covers the basic to run and test this app.

Things you may want to cover:

* Ruby version: 2.4.6

* System dependencies:
`bundle install`

* Database:
SQlite3

* How to run the test suite
`bundle exec rspec`

* API V1 endpoints:
##### Breeds
- GET localhost:3000/api/v1/breeds 
- GET localhost:3000/api/v1/breeds/1
- POST localhost:3000/api/v1/breeds
```
{
	"breed": {
    	"name": "foo"
    }
}
```
- PATCH localhost:3000/api/v1/breeds/1
```
{
	"breed": {
    	"name": "foobar"
    }
}
```
##### Dogs
- GET localhost:3000/api/v1/dogs 
- GET localhost:3000/api/v1/dogs/1
- POST localhost:3000/api/v1/dogs
```
{
	"dog": {
    	"name": "foo"
    }
}
```
- PATCH localhost:3000/api/v1/dogs/1
```
{
	"dog": {
    	"name": "foobar"
    }
}
```
## TODOS
- Caching
- Rest of summarized data
- Reservations
- Filtering

## Notes
 - I think it would've been nice to use Elasticsearch for filtering. I ran out of time to create a dockerized container to run 
an ES instance.
- Rate Limiting/Throttling also would've been nice to add.


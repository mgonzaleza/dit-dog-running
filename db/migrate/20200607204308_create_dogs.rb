# frozen_string_literal: true

# Create Dogs table
class CreateDogs < ActiveRecord::Migration[5.2]
  def change
    create_table :dogs do |t|
      t.string :name, null: false
      t.integer :age
      t.integer :weight
      t.timestamp :reservation_date_limit

      t.references :breed, foreign_key: true, null: false, index: true

      t.timestamps
    end
  end
end
# frozen_string_literal: true

# Create Breeds table
class CreateBreeds < ActiveRecord::Migration[5.2]
  def change
    create_table :breeds do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end

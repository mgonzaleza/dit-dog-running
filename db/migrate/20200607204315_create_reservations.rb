class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.integer :days

      t.references :dog, foreign_key: true, null: false, index: true

      t.timestamps
    end
  end
end

# frozen_string_literal: true

(1..340).to_a.each do |number|
  Breed.find_or_create_by(name: "breed-#{number}")
end
